package aero.tddsort;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

public class AppTest {

	private BubbleSorter bubbleSorter;

	@Before
	public void initSorter() {
		bubbleSorter = new BubbleSorter();
	}

	@Test(expected = NullPointerException.class)
	public void testNullArray() {
		bubbleSorter.sort(null);
	}

	@Test
	public void testEmptyArray() {
		int[] emptyArray = new int[] {};
		assertArrayEquals(emptyArray, bubbleSorter.sort(emptyArray));
	}

	@Test
	public void testArrayWithOneElement() {
		int[] array = new int[] { 1 };
		assertArrayEquals(array, bubbleSorter.sort(array));
	}

	@Test
	public void testSortMethodWithTwoUnsortedElements() {
		int[] initialArray = new int[] { 2, 1 };
		int[] expectedArray = new int[] { 1, 2 };
		assertArrayEquals(expectedArray, bubbleSorter.sort(initialArray));
	}

	@Test
	public void testSortMethodWithTwoSortedElements() {
		int[] initialArray = new int[] { 3, 4 };
		int[] expectedArray = new int[] { 3, 4 };
		assertArrayEquals(expectedArray, bubbleSorter.sort(initialArray));
	}

	@Test
	public void testSortMethodWithUnsortedElements() {
		int[] initialArray = new int[] { 1, 5, 9, 6, 7, 8 };
		int[] expectedArray = new int[] { 1, 5, 6, 7, 8, 9 };
		assertArrayEquals(expectedArray, bubbleSorter.sort(initialArray));
	}

}
