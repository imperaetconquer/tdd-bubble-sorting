package aero.tddsort;

public class BubbleSorter {

	public int[] sort(int[] array) {
		for (int i = 0; i < array.length - 1; i++)
			for (int j = 0; j < array.length - i - 1; j++)
				if (array[j] > array[j + 1]) {
					swapElements(array, j, j + 1);
				}
		return array;
	}

	private void swapElements(int[] array, int firstElementIndex, int secondElementIndex) {
		int temp = array[firstElementIndex];
		array[firstElementIndex] = array[secondElementIndex];
		array[secondElementIndex] = temp;
	}

}
